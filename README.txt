
-- SUMMARY --

Ubercat Affiliate2 Coupon allows user with to assign an affiliate user to a coupon. When this last is use the affiliate user will be credited with commission assigned to him thought the Affiliate2 module.


-- REQUIRENENTS --

EPSACrop depends on these modules
 - Ubercart Coupon (uc_coupon)
 - Ubercart Affiliate2 (uc_affiliate2)

-- INSTALLATION --

1. Extract uc_a2coupon on your module directory (ex. sites/all/modules)
2. Go to admin/build/modules and enable Ubercart Affiliate Coupon
3. On Coupon settings, add an affiliate on the Affiliate fieldset

-- CONFIGURATION --

Nothing to configure (at least now)

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Yvan Marques (yvmarques) - http://drupal.org/user/298685